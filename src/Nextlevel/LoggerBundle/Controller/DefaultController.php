<?php

namespace Nextlevel\LoggerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction(Request $request, $limit = 0, $page = 1, $orderBy = '', $asc = 0)
    {
        return $this->render('NextlevelLoggerBundle:Default:index.html.twig', array(
                                                'limit' => $limit,
                                                'page'  => $page,
                                                'order_by' => $orderBy,
                                                'asc'   => $asc
                                                ));
    }
}
